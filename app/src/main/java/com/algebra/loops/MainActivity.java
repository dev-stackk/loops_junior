package com.algebra.loops;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button bFor;
    private Button bWhile;
    private Button bDoWhile;
    private TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidgets();
        setupListeners();
    }

    private void initWidgets() {
        bFor = findViewById(R.id.bFor);
        bDoWhile = findViewById(R.id.bDoWhile);
        bWhile = findViewById(R.id.bWhile);
        tvResult = findViewById(R.id.tvResult);
    }

    private void setupListeners() {

        bWhile.setOnClickListener(view -> {
            int a = 0;

            while (a < 10) {
                //printText("While loop: " + a + "\n");
                a++;
            }

            int b = 100;

            while (b > 0) {
                if (b % 2 == 0) {
                    // printText("B: " + b + "\t");
                }
                b--;
            }

            b = 100;

            while (b > 0) {
                if (b % 2 == 0) {
                    printText("B: " + b + "\t");
                }

                if (b == 50) {
                    break;
                }
                b--;
            }

            b = 100;

            while (b > 0) {
                if (b % 10 == 0) {
                    printText("B: " + b);
                }

                if (b <= 30) {
                    break;
                }

                b--;
            }

        });

        bDoWhile.setOnClickListener(view -> {
            int a = 0;

            do {
                //printText("While loop: " + a + "\n");
                a++;
            } while (a < 10);

            int b = 100;

            do {
                if (b % 2 == 0) {
                    //printText("B: " + b + "\t");
                }
                b--;
            } while (b > 0);

            b = 100;

            do {
                if (b % 3 == 0) {
                    printText(b + "");
                }
                b--;
            } while (b > 0);

            b = 100;

            do {
                if (b % 2 == 0) {
                    //printText("B: " + b + "\t");
                }

                if (b == 50) {
                    break;
                }
                b--;
            } while (b > 0);


            b = 100;

            do {
                if (b % 10 == 0) {
                    //printText("B: " + b);
                }

                if (b <= 30) {
                    break;
                }

                b--;
            } while (b > 0);

        });

        bFor.setOnClickListener(view -> {

            for (int i = 0; i < 10; i++) {
                //printText("I: " + i + "\t");

                if (i%2 == 0) {
                    //printText("\n");
                }
            }

            for (int i = 10; i < 20; i++) {
                printText(i + 1 + "");
            }


            for (int i = 0; i < 100; i ++) {
                if (i == 40) {
                    continue;
                }

                if (i % 2 == 0) {
                    printText("I: " + i + "\t");
                }

                if (i == 50) {
                    break;
                }
            }
        });
    }

    private void printText(String text) {
        tvResult.append(text);
    }

}
